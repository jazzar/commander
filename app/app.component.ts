import { Component } from '@angular/core';
import {MdDialog, MdDialogRef} from '@angular/material';


export class Command {
  name: string;
  tags: string[];
}

const COMMANDS: Command[] = [
  { name: 'npm install && bower install', tags: ['npm','bower'] },
  { name: 'ng new project', tags: ['angular','cli','ng'] },
  { name: 'npm start', tags: ['npm'] }
];

@Component({
  selector: 'my-app',
  templateUrl: './app/templates/app.component.html',
  styles: [`
  
  .content{
	margin-top:20px;
  }
  

 
  span.title {
		font-family: 'Amatic SC', cursive;
		font-size: xx-large;
		font-weight:bold;
}
    
  `],
})





export class AppComponent  { 

	constructor(public dialog: MdDialog) {}

  openDialog() {
    this.dialog.open(DialogOverviewExampleDialog);
  }

	title = 'Commander';
	commands = COMMANDS;

 }
 
 
 @Component({
  selector: 'dialog-overview-example-dialog',
  template: `
		<h1 class="md-dialog-title">hello!</h1>
  `,
})
export class DialogOverviewExampleDialog {}
 
 
 
 
 
 
 
